package com.devcamp.danh_sach_san_pham.controller;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.apache.bcel.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.danh_sach_san_pham.model.CProduct;
import com.devcamp.danh_sach_san_pham.repository.ProductRepository;


@RestController
@CrossOrigin
public class ProductController {

    @Autowired 
    ProductRepository productRepository;

    @GetMapping("/productions")
    public ResponseEntity<List<CProduct>> getProductsAll(){
       

        try {

            List<CProduct> products = new ArrayList<CProduct>();
            productRepository.findAll().forEach(products::add);
            return new ResponseEntity<>(products , HttpStatus.OK);
            
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    
}
