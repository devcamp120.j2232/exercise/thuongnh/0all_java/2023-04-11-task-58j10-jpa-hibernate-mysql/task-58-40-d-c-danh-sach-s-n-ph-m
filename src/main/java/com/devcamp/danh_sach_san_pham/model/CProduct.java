package com.devcamp.danh_sach_san_pham.model;

import javax.persistence.*;


@Entity  // kết nối được với sql data base
@Table(name = "products")  // ten cho data base
public class CProduct {

    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    
    private long id;

    @Column(name= "ten_san_pham")
    private String tenSanPham;

    @Column(name= "ma_san_pham")
    private String maSanPham;

    @Column(name= "gia_tien")
    private int giaTien;

    @Column(name= "ngay_tao")
    private long ngayTao;

    @Column(name= "ngay_cap_nhat")
    private long ngayCapNhat;

    public CProduct() {
    }

    public CProduct(long id, String tenSanPham, String maSanPham, int giaTien, long ngayTao, long ngayCapNhat) {
        this.id = id;
        this.tenSanPham = tenSanPham;
        this.maSanPham = maSanPham;
        this.giaTien = giaTien;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public int getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(int giaTien) {
        this.giaTien = giaTien;
    }

    public long getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public long getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    @Override
    public String toString() {
        return "CProduct [id=" + id + ", tenSanPham=" + tenSanPham + ", maSanPham=" + maSanPham + ", giaTien=" + giaTien
                + ", ngayTao=" + ngayTao + ", ngayCapNhat=" + ngayCapNhat + "]";
    }
    
    


    


    
}
