package com.devcamp.danh_sach_san_pham.repository;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import com.devcamp.danh_sach_san_pham.model.CProduct;

public interface ProductRepository  extends JpaRepositoryImplementation<CProduct, Long> {
    
}
 