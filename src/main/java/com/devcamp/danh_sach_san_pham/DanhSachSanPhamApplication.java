package com.devcamp.danh_sach_san_pham;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanhSachSanPhamApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanhSachSanPhamApplication.class, args);
	}

}
